# use Selenium::Remote::Driver;
use Selenium::Firefox;

# Create a new instance of the driver
# my $driver = Selenium::Remote::Driver->new;
my $driver = Selenium::Firefox->new;

# Set the timeout for searching for elements to 10 seconds (0 by default)
$driver->set_implicit_wait_timeout(10000);

# Go to the google homepage
$driver->get("http://www.google.com");

# Print original page title
my $title = $driver->get_title();
print "$title\n";

# Find the element that's name attribute is q (google search box)
my $inputElement = $driver->find_element("q", "name");

# type in the search
$inputElement->send_keys("cheese!");

# submit the form (although google automatically searches now without submitting)
$inputElement->submit();

# my $path = '//h3[@class="r"][1]/a[1]/@href';
my $path = 'h3.r > a';
my $strategy = 'css';

my $first_result_url = $driver->find_element($path, $strategy);

print "$first_result_url\n";

$driver->quit();
