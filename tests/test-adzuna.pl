#!/usr/bin/perl
use strict;
use warnings;

use Test::More;
use Selenium::Firefox;


=pod
Basic Firefox testing with Selenium::Firefox
=cut

my $driver = Selenium::Firefox->new(
    extra_capabilities => {
        'moz:firefoxOptions' => {
            args => ['-headless'],
        },
    },
);
isa_ok($driver, 'Selenium::Firefox',
    'The driver is loaded'
);

my $geckodriver_port = $driver->port;
ok( $geckodriver_port,
    'We have the geckodriver port'
);

my @geckodriver_pids = geckodriver_pid_from_port( $geckodriver_port );
ok( @geckodriver_pids == 1,
    'There is only one geckodriver process on port: ' . $geckodriver_port
);

my $geckodriver_pid = $geckodriver_pids[0];
ok( $geckodriver_pid,
    'Got the geckodriver pid'
);

$driver->get( 'https://www.adzuna.co.uk' );
my $title = $driver->get_title();
is( $title, 'Jobs in London, the UK & Beyond | Adzuna',
    'We got the page title'
);

my $h1 = $driver->find_element('h1', 'css');
is( $h1->get_text(), 'Adzuna',
    'Found <h1> text'
);

$driver->shutdown_binary();

# The stock geckodriver executable will not exit by the above, see:
# https://github.com/mozilla/geckodriver/issues/771
#
# But, if the patched geckodriver is used, then it will shut down
# by the command above and there is no need to send a kill signal
# my $signaled = kill 'SIGKILL', $geckodriver_pid;
# ok( $signaled,
#     'Sent a SIGKILL to the geckodriver process'
# );


done_testing();

##########################
sub geckodriver_pid_from_port
{
    my $port = shift;

    use Proc::ProcessTable;
    my $t = Proc::ProcessTable->new;
    my @driver_processes =
        grep {$_->cmndline =~ /geckodriver.*--port\s+$port/} @{$t->table};

    my @driver_pids = map {$_->pid} @driver_processes;

    return @driver_pids;
}
