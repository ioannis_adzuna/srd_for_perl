use Selenium::Firefox;


my $driver = Selenium::Firefox->new(

#    marionette_enabled => 0,

#    custom_args        => '-P default',  # Use the 'testing' Firefox profile
#    custom_args => '--headless',

);

# This will wait for at most 10sec for a requested DOM element to appear

# If after 10 seconds it does not, it will croak

$driver->set_implicit_wait_timeout(10_000);


# Load the following page

$driver->get('http://careers.rolls-royce.com/united-kingdom/job-search-results');


# Extract the list of 'a' elements for all the ads

my @links = $driver->find_elements('div.corporate-site-link>a', 'css');


# Get the list of ad ids in order to construct the ad URLs

my @ids = map {$_->get_text} @links;


print "@ids\n";