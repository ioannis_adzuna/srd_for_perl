# srd_for_perl

## Description 

**Note** this docker machine is for Linux-based X11 hosts.

A Docker image combining Ubuntu, Perl, Firefox, Geckodriver and 
Selenium::Firefox module for development and testing.

Specifically:

| Software          | Default Version | Build Parameter (```--build-arg```) |
|-------------------|----------------:|:------------------------------------|
| Ubuntu            | 14.04           | *n/a*                               |
| Perl              | 5.18.2          | perl_version                        |
| Firefox           | 60.0            | firefox_version                     |
| Geckodriver       | 0.21.0          | geckodriver_version                 |
| Selenium::Firefox | 1.28            | selenium_firefox_module_version     |

## Requirements

+ Linux host
+ X Window System

## Usage

1. Download / clone

2. Build: ```./build.sh```

3. Run: ```./run.sh```

It should open a bash shell prompt.

4. List tests: ```ls tests```

5. Execute tests. E.g.: ```perl tests/tests-rollsroyce-com.pl```

The script visits [Rolls-Royce.com](http://careers.rolls-royce.com/united-kingdom/job-search-results) and captures the inner text of the element ```css div.corporate-site-link>a```

After a while a Firefox window should pop up and the phrase 
```Rolls-Royce.com``` should be put on terminal.
