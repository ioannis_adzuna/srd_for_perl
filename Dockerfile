FROM ubuntu:14.04

# BUILD ARGUMENTS
# overriden at build time: --build-arg arg=value

ARG user_id=1000
ARG group_id=1000
ARG user_name='developer'
ARG user_full_name='Developer'
ARG user_shell='/bin/bash'
ARG user_home="/home/${user_name}"
ARG firefox_version="60.0"
ARG perl_version="5.18.2"
ARG geckodriver_version="0.21.0"
ARG selenium_firefox_module_version="1.28"



# BASE SYSTEM UPDATE

ENV DEBIAN_FRONTEND noninteractive

RUN locale-gen en_US.UTF-8 && dpkg-reconfigure locales

# update/install necessary software
RUN true \
    && apt-get update \
    # uninstall firefox (if any)
    && sudo apt-get purge firefox \
    # && \
    && apt-get install -y --no-install-recommends \
        # building purposes
        bzip2 \
        build-essential \
        git \
        tar \
        # certificates for secure downloading
        ca-certificates \
        # as http downloader
        wget \
        # required by firefox
        libdbus-glib-1-dev \
        libgtk-3-dev \
        libpango-1.0-0 \
        libx11-xcb1 \
        libxt6 \
    && rm -rf /var/lib/apt/lists/*



# SETUP FIREFOX

ENV FIREFOX_VERSION ${firefox_version}
WORKDIR /tmp
RUN true \
    && wget http://ftp.mozilla.org/pub/firefox/releases/${FIREFOX_VERSION}/linux-$(uname -m)/en-US/firefox-${FIREFOX_VERSION}.tar.bz2 \
    && tar -xjf firefox-${FIREFOX_VERSION}.tar.bz2 \
    && mv firefox /opt/ \
    # && mv /usr/bin/firefox /usr/bin/firefox_old
    && ln -s /opt/firefox/firefox /usr/bin/firefox \
    && rm -f firefox-${FIREFOX_VERSION}.tar.bz2
WORKDIR /



# SETUP DEVELOPER USER
# TODO: parameterize according to ARGs

ENV DOCKER_USER_ID ${user_id}
ENV DOCKER_GROUP_ID ${group_id}

# create the proper records for user
RUN export uid=$DOCKER_USER_ID gid=$DOCKER_GROUP_ID && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

ENV HOME /home/developer

USER developer

WORKDIR $HOME

COPY ./user/conf/.bashrc ./



# SETUP PERLBREW & PERL

ENV PERL_VERSION ${perl_version}

# download Perlbrew
RUN \wget -O - https://install.perlbrew.pl | bash

# enable Perlbrew terminal tools on shell login
RUN echo "source ~/perl5/perlbrew/etc/bashrc" >> ~/.bash_profile

# update patchperl
RUN echo "y" | $HOME/perl5/perlbrew/bin/perlbrew install-patchperl

# use VM version of Perl
RUN $HOME/perl5/perlbrew/bin/perlbrew install perl-$PERL_VERSION

# setup environment according to Perlbrew for the proper Perl version

ENV MANPATH $HOME/perl5/perlbrew/perls/perl-$PERL_VERSION/man

# ENV PERL5LIB $HOME/perl5/lib/perl5:$HOME/perl5/perlbrew/perls/perl-$PERL_VERSION/lib/$PERL_VERSION
ENV PERL5LIB $HOME/perl5/lib/perl5:${PERL5LIB}

ENV PERLBREW_LIB ''
ENV PERLBREW_MANPATH $HOME/perl5/perlbrew/perls/perl-$PERL_VERSION/man
ENV PERLBREW_PATH $HOME/perl5/perlbrew/bin:$HOME/perl5/perlbrew/perls/perl-$PERL_VERSION/bin
ENV PERLBREW_PERL perl-$PERL_VERSION
ENV PERLBREW_ROOT $HOME/perl5/perlbrew
ENV PERLBREW_SKIP_INIT 1
ENV PERLBREW_VERSION 0.84
ENV PERL_LOCAL_LIB_ROOT ''
ENV SHELL /bin/bash
ENV PERL_MB_OPT "--install_base \"${HOME}/perl5\""
ENV PERL_MM_OPT "INSTALL_BASE=${HOME}/perl5"

#important: it provides default answers for CPAN
ENV PERL_MM_USE_DEFAULT 1

ENV PATH $HOME/perl5/perlbrew/perls/perl-$PERL_VERSION/bin:${PATH}
ENV PATH $HOME/perl5/bin:${PATH}

# ADD ./MyConfig.pm $HOME/.cpan/CPAN/
# ENV CPAN_CONFIG $HOME/.cpan/CPAN/MyConfig.pm


# install cpanm in perlbrew
RUN cpan App::cpanminus

# install dependencies for Selenium
RUN cpanm LWP::UserAgent

# install Selenium
# install Adzuna specific version
ENV SELENIUM_FIREFOX_MODULE_VERSION ${selenium_firefox_module_version}
RUN cpanm TEODESIAN/Selenium-Remote-Driver-${SELENIUM_FIREFOX_MODULE_VERSION}.tar.gz
# it should also install all dependencies starting for Selenium::Remote::Driver

# install process table module
RUN cpanm Proc::ProcessTable




# SETUP GECKODRIVER

ENV GECKODRIVER_VERSION ${geckodriver_version}
WORKDIR /tmp
RUN true \
    && wget https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VERSION}/geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz \
    && tar -xvzf geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz \
    && sudo mkdir -p /opt/geckodriver \
    && sudo cp geckodriver /opt/geckodriver/ \
    && sudo ln -s /opt/geckodriver/geckodriver /usr/bin/geckodriver \
    && rm geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz
WORKDIR $HOME



# SETUP RUNTIME DEV TOOLS
RUN true \
    && sudo apt-get update \
    && sudo apt-get install -y --no-install-recommends \
        vim \
    && sudo rm -rf /var/lib/apt/lists/*


# ADD TEST PERL FILES

COPY ./tests ./tests

USER root
RUN chown -R $USER:$GROUP tests
USER $user_name



# ENTRYPOINT

CMD /bin/bash